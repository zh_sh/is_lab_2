﻿using Microsoft.EntityFrameworkCore;
using MovieApp.Data;
using MovieApp.Domain;
using MovieApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.Repository.Implementations
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext context;
        private DbSet<EShopApplicationUser> entities;
        string errorMessage = string.Empty;

        public UserRepository(ApplicationDbContext context)
        {
            this.context = context;
            entities = context.Set<EShopApplicationUser>();
        }

        public IEnumerable<EShopApplicationUser> GetAll()
        {
            return entities.AsEnumerable();
        }

        public EShopApplicationUser Get(string id)
        {
            //return entities.FirstOrDefault(e => e.Id == id);
            var strGuid = id.ToString();
            //return entities.Include(z => z.UserCart).Include(z => z.UserCart.ProductsInShoppingCarts).Include("UserCart.ProductsInShoppingCarts.Product").SingleOrDefault(s => s.Id == strGuid);
            //maybe something with ProductsInShoppingCarts idk
            return entities.Include(z => z.Order).Include(z => z.Order.TicketInOrders).Include("Order.TicketInOrders.Ticket").SingleOrDefault(s => s.Id == strGuid);
        }

        public void Insert(EShopApplicationUser entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            context.SaveChanges();
        }

        public void Update(EShopApplicationUser entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Update(entity);
            context.SaveChanges();
        }

        public void Delete(EShopApplicationUser entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
            context.SaveChanges();
        }
    }
}
