﻿using MovieApp.Domain.DTOs;
using MovieApp.Domain.Models;
using MovieApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.Service.Interfaces
{
    public interface IOrderService
    {
        OrderDTO getOrderInfo(string userId);
        bool deleteTicketFromOrder(string userId, Guid productId);
        bool order(string userId);
        bool AddToOrderConfirmed(TicketInOrder model, string userId);
    }
}
