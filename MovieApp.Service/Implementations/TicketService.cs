﻿using Microsoft.Extensions.Logging;
using MovieApp.Domain.Models;
using MovieApp.Models;
using MovieApp.Repository.Interfaces;
using MovieApp.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.Service.Implementations
{
    public class TicketService : ITicketService
    {

        private readonly IRepository<Ticket> _ticketRepository;
        private readonly IRepository<TicketInOrder> _ticketInOrderRepository;
        private readonly IUserRepository _userRepository;
        private readonly ILogger<TicketService> _logger;

        public TicketService(IRepository<Ticket> ticketRepository, IRepository<TicketInOrder> ticketInOrderRepository, IUserRepository userRepository, ILogger<TicketService> logger)
        {
            _ticketRepository = ticketRepository;
            _ticketInOrderRepository = ticketInOrderRepository;
            _userRepository = userRepository;
            _logger = logger;
        }

        public void CreateNewTicket(Ticket t)
        {
            _ticketRepository.Insert(t);
        }

        public void DeleteTicket(Guid id)
        {
            Ticket ticket = _ticketRepository.Get(id);
            _ticketRepository.Delete(ticket);
            //_ticketRepository.Delete(GetDetailsForTicket(id)); //this was suggested through autocompletion
        }

        public List<Ticket> GetAllTickets()
        {
            return _ticketRepository.GetAll().ToList();
        }

        public Ticket GetDetailsForTicket(Guid? id)
        {
            return _ticketRepository.Get(id);
        }

        public void UpdeteExistingTicket(Ticket t)
        {
            _ticketRepository.Update(t);
        }
    }
}
