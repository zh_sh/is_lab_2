﻿using MovieApp.Domain.DTOs;
using MovieApp.Domain.Models;
using MovieApp.Repository.Interfaces;
using MovieApp.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.Service.Implementations
{
    public class OrderService : IOrderService
    {

        //private readonly IRepository<ShoppingCart> _shoppingCartRepository;
        private readonly IRepository<Order> _orderRepository;
        //private readonly IRepository<EmailMessage> _mailRepository;
        private readonly IRepository<TicketInOrder> _ticketInOrderRepository;
        //private readonly IRepository<ProductsInShoppingCart> _productInShoppingCartRepository;
        private readonly IUserRepository _userRepository;
        //private readonly IEmailService _emailService;

        public OrderService(IRepository<Order> orderRepository, IRepository<TicketInOrder> ticketInOrderRepository, IUserRepository userRepository)
        {
            _orderRepository = orderRepository;
            _ticketInOrderRepository = ticketInOrderRepository;
            _userRepository = userRepository;
        }

        public bool AddToOrderConfirmed(TicketInOrder model, string userId)
        {
            var user = this._userRepository.Get(userId);
            var shoppingCart = user.Order;
            TicketInOrder itemToAdd = new TicketInOrder
            {
                Id = Guid.NewGuid(),
                Ticket = model.Ticket,
                TicketId = model.TicketId,
                //ShoppingCart = shoppingCart,
                //ShoppingCartId = shoppingCart.Id,
                Order = model.Order,
                OrderId = model.OrderId,
                Quantity = model.Quantity
            };

            _ticketInOrderRepository.Insert(itemToAdd);
            return true;
        }

        public bool deleteTicketFromOrder(string userId, Guid ticketId)
        {
            if (!string.IsNullOrEmpty(userId) && ticketId != null)
            {
                //var loggedInUser = this._userRepository.Get(userId);
                //var userShoppingCart = loggedInUser.UserCart;
                //var itemToDelete = userShoppingCart.ProductsInShoppingCarts.Where(z => z.ProductId.Equals(productId)).FirstOrDefault();
                //userShoppingCart.ProductsInShoppingCarts.Remove(itemToDelete);
                //this._orderRepository.Update(userShoppingCart);
                //return true;
                var loggedInUser = this._userRepository.Get(userId);
                var userOrder = loggedInUser.Order;
                var itemToDelete = userOrder.TicketInOrders.Where(z => z.TicketId
                .Equals(ticketId)).FirstOrDefault();
                userOrder.TicketInOrders.Remove(itemToDelete);
                this._orderRepository.Update(userOrder);
                return true;
            }
            return false;
        }

        public OrderDTO getOrderInfo(string userId)
        {
            var user = this._userRepository.Get(userId);
            //var shoppingCart = user.Order;
            var userOrder = user.Order;

            OrderDTO model = new OrderDTO()
            {
                TicketInOrders = userOrder.TicketInOrders ?? new List<TicketInOrder>(),
                TotalPrice = userOrder.TicketInOrders.Sum(z => z.Quantity * z.Ticket.Price)

            };
            return model;
        }

        public bool order(string userId)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                var loggedInUser = this._userRepository.Get(userId);
                var userOrder = loggedInUser.Order;

                //EmailMessage message = new EmailMessage();
                //message.Subject = "Successfull order";
                //message.MailTo = loggedInUser.Email;

                Order order = new Order
                {
                    Id = Guid.NewGuid(),
                    EshopApplicationUserId = Guid.Parse(userId)
                };
                this._orderRepository.Insert(order);

                List<TicketInOrder> productInOrders = new List<TicketInOrder>();

                //dangerous territory
                //var result = userOrder.ProductsInShoppingCarts.Select(z => new ProductsInOrders
                var result = userOrder.TicketInOrders.Select(z => new TicketInOrder
                {
                    Id = Guid.NewGuid(),
                    TicketId = z.Ticket.Id,
                    Ticket = z.Ticket,
                    OrderId = order.Id,
                    Order = order,
                    Quantity = z.Quantity
                }).ToList();

                StringBuilder sb = new StringBuilder();

                var totalPrice = 0.0;

                //sb.AppendLine("Your order is completed. The order conatins: ");

                //for (int i = 1; i <= result.Count(); i++)
                //{
                //    var currentItem = result[i - 1];
                //    totalPrice += currentItem.Quantity * currentItem.Ticket.Price;
                //    sb.AppendLine(i.ToString() + ". " + currentItem.Ticket.Movie.MovieName + " with quantity of: " + currentItem.Quantity + " and price of: $" + currentItem.Ticket.Price);
                //}

                //sb.AppendLine("Total price for your order: " + totalPrice.ToString());
                //message.Content = sb.ToString();


                productInOrders.AddRange(result);

                foreach (var item in productInOrders)
                {
                    this._ticketInOrderRepository.Insert(item);
                }

                //loggedInUser.UserCart.ProductsInShoppingCarts.Clear();
                loggedInUser.Order.TicketInOrders.Clear();

                this._userRepository.Update(loggedInUser);
                //this._mailRepository.Insert(message);
                //this._emailService.SendEmailAsync(message);
                return true;
            }

            return false;
        }
    }
}
