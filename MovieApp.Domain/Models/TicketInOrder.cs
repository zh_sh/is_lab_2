﻿using MovieApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.Domain.Models
{
    public class TicketInOrder : BaseEntity
    {
        //[Key]
        //public Guid Id { get; set; }

        public Guid TicketId { get; set; }
        public Ticket? Ticket { get; set; }


        public Guid OrderId { get; set; }
        public Order? Order { get; set; }

        public int Quantity { get; set; }

    }
}
