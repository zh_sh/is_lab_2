﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.Domain.Models
{
    public class Order : BaseEntity
    {
        //[Key]
        //public Guid Id { get; set; }
        public Guid EshopApplicationUserId { get; set; }
        public virtual List<TicketInOrder>? TicketInOrders { get; set; } //idk which one..list or icollection
        //public virtual ICollection<TicketInOrder>? TicketInOrders { get; set; } //anything that uses / has a DTO is supposed to use a list instead of ICollection?

        public double TotalPrice { get; set; }
    }
}
