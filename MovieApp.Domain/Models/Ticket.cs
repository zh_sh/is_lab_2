﻿using System.ComponentModel.DataAnnotations;
using MovieApp.Domain;
using MovieApp.Domain.Models;

namespace MovieApp.Models
{
    public class Ticket : BaseEntity
    {
        //[Key]
        //public Guid Id { get; set; }
        [Required]
        public double Price { get; set; }
        public Guid MovieId { get; set; }
        public Movie? Movie { get; set; } //shouldn't it be virtual like the rest / other cases?...or is virtual _just for appdbcontext_
        //public virtual Movie? Movie { get; set; }
        public virtual EShopApplicationUser? CreatedBy { get; set; }
        public virtual ICollection<TicketInOrder>? TicketInOrders { get; set; }

    }
}
