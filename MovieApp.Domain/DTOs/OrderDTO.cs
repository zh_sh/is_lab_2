﻿using MovieApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.Domain.DTOs
{
    public class OrderDTO
    {
        //public Guid EshopApplicationUserId { get; set; }
        //public virtual ICollection<TicketInOrder>? TicketInOrders { get; set; }
        public virtual List<TicketInOrder>? TicketInOrders { get; set; }
        public double TotalPrice { get; set; }
    }
}
