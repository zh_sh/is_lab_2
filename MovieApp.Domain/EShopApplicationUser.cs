﻿using Microsoft.AspNetCore.Identity;
using MovieApp.Domain.Models;
using MovieApp.Models;

namespace MovieApp.Domain
{
    public class EShopApplicationUser : IdentityUser
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Address { get; set; }
        public virtual ICollection<Ticket>? MyTickets { get; set; }
        //public virtual ICollection<Order>? MyOrders { get; set; } //inconsistant accessibility bs...nvm it must be a 1-to-1 relationship anyway
        public virtual Order? Order { get; set; }
    }

}
